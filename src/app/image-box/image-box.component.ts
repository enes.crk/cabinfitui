import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EnterMeasureModalComponent } from '../enter-measure-modal/enter-measure-modal.component';

@Component({
  selector: 'image-box',
  templateUrl: './image-box.component.html',
  styleUrls: ['./image-box.component.scss']
})
export class ImageBoxComponent implements OnInit {

  @Input() url;
  @Input() turnBack;
  @Output() previousPage: EventEmitter<any> = new EventEmitter();
  @Output() reset: EventEmitter<any> = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  goToPreviousPage(){
    this.previousPage.emit(true);
  }

  resetAll(){
    this.reset.emit(true);
  }
  enterMeasure(){
    const dialogRef = this.dialog.open(EnterMeasureModalComponent, {
      width: "350px",
      data: { text: "Göğüs"},
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }
}