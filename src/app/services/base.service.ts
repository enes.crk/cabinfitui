import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class BaseService {
  apiUrl: string; // URL

  constructor(private http: HttpClient) {
    this.apiUrl = this.getAPIUrl();
  }

  private getAPIUrl() {
    return "https://cabinfitapi.cabin.com.tr/api";
    //for test environment
    // return "https://cabinfittestapi.cabin.com.tr/api";
  }
}
