import { Injectable } from '@angular/core';
import swal from "sweetalert2";
import { TranslateService } from '@ngx-translate/core';
import { map, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor(
    private translate: TranslateService
  ) { }

  successMessage(title: string, name = '') {
    return this.translate.get(title, { name: name }).pipe(
      map(res => res),
      mergeMap(res => {
        return swal.fire({
          type: 'success',
          title: res,
          showConfirmButton: false,
          timer: 1500
        });
      })
    )
  }

  newSuccessMessage(text: string) {
    return swal.fire({
      type: 'success',
      title: text,
      // showConfirmButton: false,
      timer: 1500
    });
  }

  confirmMessage(title: string, text = '', confirmButtonText = "Sil", cancelButtonText = "Vazgeç") {
    return swal.fire({
      type: 'question',
      title: title,
      text: text,
      showConfirmButton: true,
      showCancelButton: true,
      focusCancel: true,
      confirmButtonColor: '#d33',
      confirmButtonText: confirmButtonText,
      cancelButtonText: cancelButtonText
    });
  }

  infoMessage(title: string, email = '') {
    return this.translate.get([title, 'ok'], { address: email }).pipe(
      map(res => res),
      mergeMap(res => {
        return swal.fire({
          type: 'info',
          title: res.checkemail,
          showConfirmButton: true,
          confirmButtonText: res.ok
        });
      })
    )
  }

  errorMessage(title: string) {
    return this.translate.get([title, 'ok']).pipe(
      map(res => res),
      mergeMap(res => {
        return swal.fire({
          type: 'error',
          title: res.title,
          showConfirmButton: true,
          confirmButtonText: res.ok,
          timer: 1500
        });
      })
    )
  }

  confirmDeleteMessage() {
    return this.translate.get(['confirmdeletetitle', 'reallysuretext', 'delete', 'cancel']).pipe(
      map(res => res),
      mergeMap(messages => {
        return swal.fire({
          type: 'question',
          title: messages.confirmdeletetitle,
          text: messages.reallysuretext,
          showConfirmButton: true,
          showCancelButton: true,
          focusCancel: true,
          confirmButtonColor: '#d33',
          confirmButtonText: messages.delete[0].toUpperCase() + messages.delete.substring(1, messages.delete.length),
          cancelButtonText: messages.cancel[0].toUpperCase() + messages.cancel.substring(1, messages.cancel.length),
        });
      })
    )
  }

  confirmBlockMessage() {
    return this.translate.get(['confirmblocktitle', 'reallysuretext', 'block', 'cancel']).pipe(
      map(res => res),
      mergeMap(messages => {
        return swal.fire({
          type: 'question',
          title: messages.confirmblocktitle,
          text: messages.reallysuretext,
          showConfirmButton: true,
          showCancelButton: true,
          focusCancel: true,
          confirmButtonColor: '#d33',
          confirmButtonText: messages.block[0].toUpperCase() + messages.block.substring(1, messages.block.length),
          cancelButtonText: messages.cancel[0].toUpperCase() + messages.cancel.substring(1, messages.cancel.length),
        });
      })
    )
  }

  actionWithNote(title: string, confirmButtonText: string, confirmButtonColor = '#d33') {
    return swal.fire({
      type: 'question',
      title: title,
      text: "Bu işlem için not girmeniz zorunludur.",
      showConfirmButton: true,
      showCancelButton: true,
      focusCancel: true,
      confirmButtonColor: confirmButtonColor,
      confirmButtonText: confirmButtonText,
      cancelButtonText: "İptal",
      input: "textarea",
      inputValidator: (value) => {
        return !value && 'Lütfen not giriniz!'
      },
    })
  }

  deleteMessage(title: string) {
    return this.translate.get(['deletecompleted', 'ok'], { title: title.charAt(0).toUpperCase() + title.slice(1) }).pipe(
      map(res => res),
      mergeMap(messages => {
        return swal.fire({
          type: 'info',
          text: messages.deletecompleted,
          showConfirmButton: true,
          confirmButtonText: messages.ok,
          timer: 1500
        })
      })
    )
  }

  blockMessage(title: string) {
    return this.translate.get(['blockcompleted', 'ok'], { title: title.charAt(0).toUpperCase() + title.slice(1) }).pipe(
      map(res => res),
      mergeMap(messages => {
        return swal.fire({
          type: 'info',
          text: messages.blockcompleted,
          showConfirmButton: true,
          confirmButtonText: messages.ok,
          timer: 1500
        })
      })
    )
  }

  warningMessage(text: string) {
    return this.translate.get(text).pipe(
      map(res => res),
      mergeMap(res => {
        return swal.fire({
          type: 'warning',
          text: res,
          timer: 1500
        });
      })
    )
  }

  newWarningMessage(text: string) {
    return swal.fire({
      type: 'warning',
      text: text,
      timer: 2500
    });
  }
}
