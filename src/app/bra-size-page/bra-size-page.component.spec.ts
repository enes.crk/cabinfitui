import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BraSizePageComponent } from './bra-size-page.component';

describe('BraSizePageComponent', () => {
  let component: BraSizePageComponent;
  let fixture: ComponentFixture<BraSizePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BraSizePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BraSizePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
