import { HttpClient } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { Inject } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { BraDialogComponent } from "../bra-dialog/bra-dialog.component";
import { BaseService } from "../services/base.service";

export interface DialogData {
  text: string;
  image: string;
}

@Component({
  selector: "bra-size-page",
  templateUrl: "./bra-size-page.component.html",
  styleUrls: ["./bra-size-page.component.scss"],
})
export class BraSizePageComponent implements OnInit {
  @Input() data;
  @Input() color;
  @Input() pageGuideTexts;
  @Input() gender;
  @Input() brandId;
  @Input() productId;
  @Input() customerId;
  @Output() responseModel: EventEmitter<any> = new EventEmitter();

  dialogText: string =
    "Sütyen bedeninizi sütyeninizin etiket kısmından öğrenebilirsiniz.";
  dialogImage: string =
    "https://dtpmhvbsmffsz.cloudfront.net/posts/2015/07/21/55ae9232f527081bcb01dbd1/s_55ae9240519f3a725601debe.jpg";

  braSizes = [];
  braCups = [];

  selectedBraSize: any;
  selectedBraCup: any;
  selectedBraName: any;
  selectedCupName: any;
  authorization: string = "1a0e12a3-147d-4f5a-bdae-dc588293b91e";

  constructor(public dialog: MatDialog,private httpClient: HttpClient,  private baseService: BaseService) {}

  ngOnInit(): void {
    this.set();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(BraDialogComponent, {
      width: "500px",
      data: { text: this.dialogText, image: this.dialogImage },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }

  set() {
    this.braSizes = this.data.Patterns[0].Measures;
    this.braCups = this.data.Patterns[0].Cups;
  }

  chaneBraSize(event) {
    this.selectedBraSize = event.value;
     for (let index = 0; index < this.braSizes.length; index++) {
      if(this.braSizes[index].Value == event.value){
       this.selectedBraName = this.braSizes[index].Measure
      }
     }
    
  }
  changeBraCup(event) {
    this.selectedBraCup = event.value;
     for (let index = 0; index < this.braCups.length; index++) {
       if(this.braCups[index].Value == event.value){
        this.selectedCupName = this.braCups[index].Cup
       }
   }
}

  deleteBraSelection() {
    this.selectedBraSize = "";
    this.selectedBraCup = "";
  }

  goToNextStage() {

    let reqModel = [{
      "Id":1,
      "Value": this.selectedBraSize + this.selectedBraCup
  }]
    var headers = {"Brand-Id": `${this.brandId}`, "Product-Id": `${this.productId}`, "User-Id": `${this.customerId}`, "Authorization": `Bearer ${this.authorization}`};

    this.httpClient
      .post(
        `${this.baseService.apiUrl}/Bodypart`, reqModel,
        { headers }
      )
      .subscribe({
        next: (data) => {
          this.getRequiredBodyPart();
        },
        error: (error) => {
          console.log(error);
        },
      });
  }

  getRequiredBodyPart(){
    var headers = {"Brand-Id": `${this.brandId}`, "Product-Id": `${this.productId}`, "User-Id": `${this.customerId}`, "Authorization": `Bearer ${this.authorization}`};
    let res
    this.httpClient
      .get(
        `${this.baseService.apiUrl}/Bodypart/Required`,
        { headers }
      )
      .subscribe({
        next: (data) => {
          res = data[0]
          this.responseModel.emit(res);
         
        },
        error: (error) => {
          console.log(error);
        },
      });
      
     
  }
}
