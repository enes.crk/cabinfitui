import { isNull } from "@angular/compiler/src/output/output_ast";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "recommendation-page",
  templateUrl: "./recommendation-page.component.html",
  styleUrls: ["./recommendation-page.component.scss"],
})
export class RecommendationPageComponent implements OnInit {
  @Input() color;
  @Input() data;
  @Input() pageGuideTexts;
  @Input() gender;
  @Input() brandPreferences;
  @Output() notSuitable: EventEmitter<boolean> = new EventEmitter();
  @Output() reset: EventEmitter<any> = new EventEmitter();
  normalPreferenceSizes: any = [];
  loosePreferenceSizes: any = [];
  tightPreferenceSizes: any = [];
  recommendedSizes: any = [];

  showPrimaryRecommendationDetail:boolean = false;
  showSecondaryRecommendationDetail:boolean = false;
  minimumPersentage: any = null;


  constructor() {}

  ngOnInit(): void {
    this.minimumPersentage = this.brandPreferences?.minPersantage ?? 40;
    this.getRecommendation();
    
  }

  getRecommendation() {
    let sizesObject = this.data.Product.Sizes;
    for (let index = 0; index < sizesObject.length; index++) {
      this.normalPreferenceSizes.push({
        "Name": this.data.Product.Sizes[index].Name,
        "Preference": this.data.Product.Sizes[index].Preferences[0]
      });
      this.tightPreferenceSizes.push({
        "Name": this.data.Product.Sizes[index].Name,
        "Preference": this.data.Product.Sizes[index].Preferences[1]
      });
      this.loosePreferenceSizes.push({
        "Name": this.data.Product.Sizes[index].Name,
        "Preference": this.data.Product.Sizes[index].Preferences[2]
      });
    }

    this.sortFitness();
  }

  sortFitness(){
    // Sıralı gelen sizeName leri array e atar.
    let sizeNames = this.normalPreferenceSizes.map(x => x.Name);
    // Sizeları fitness a göre sıralar
    this.normalPreferenceSizes.sort((a, b) => (a.Preference.Fitness < b.Preference.Fitness) ? 1 : -1);
    // En yüksek oranlı bedenin adının indexini bulur
    let index = sizeNames.indexOf(this.normalPreferenceSizes[0].Name);
    // bulduğu indexin alt ve üst indexlerindeki bedenleri setler
    let upperSize = sizeNames[index + 1] ?? null;
    let lowerSize = sizeNames[index - 1] ?? null;
    let tempArr = [];

    if((this.normalPreferenceSizes.filter(x => x.Name == upperSize)[0]?.Preference.Fitness) > (this.normalPreferenceSizes.filter(x => x.Name == lowerSize)[0]?.Preference.Fitness)){
      tempArr.push(this.normalPreferenceSizes[0])
      tempArr.push(this.normalPreferenceSizes.filter(x => x.Name == upperSize)[0])      
      this.normalPreferenceSizes = tempArr;
    }else if((this.normalPreferenceSizes.filter(x => x.Name == upperSize)[0]?.Preference.Fitness) < (this.normalPreferenceSizes.filter(x => x.Name == lowerSize)[0]?.Preference.Fitness)){
      tempArr.push(this.normalPreferenceSizes[0])
      tempArr.push(this.normalPreferenceSizes.filter(x => x.Name == lowerSize)[0])
      
      this.normalPreferenceSizes = tempArr;
    }else{

    }
    // this.tightPreferenceSizes.sort((a, b) => (a.Preference.Fitness < b.Preference.Fitness) ? 1 : -1);
    // this.loosePreferenceSizes.sort((a, b) => (a.Preference.Fitness < b.Preference.Fitness) ? 1 : -1);

    this.changePreference();
  }

  changePreference(preference?) {
    switch (preference) {
      case 1:
        this.recommendedSizes = this.tightPreferenceSizes;
        break;
      case 2:
        this.recommendedSizes = this.loosePreferenceSizes;
        break;
      default:
        this.recommendedSizes = this.normalPreferenceSizes;
    }
    if(this.recommendedSizes[0].Preference.Fitness <= this.minimumPersentage){
      this.notSuitable.emit(true);
    }
  }
  resetAll(){
    this.reset.emit(true);
  }
}
