import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductMeasurePageComponent } from './product-measure-page.component';

describe('ProductMeasurePageComponent', () => {
  let component: ProductMeasurePageComponent;
  let fixture: ComponentFixture<ProductMeasurePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductMeasurePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMeasurePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
