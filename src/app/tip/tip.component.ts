import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'tip',
  templateUrl: './tip.component.html',
  styleUrls: ['./tip.component.scss']
})
export class TipComponent implements OnInit {

  @Input() selectedTip;
  selectedTipText: any;
  icon = "../../assets/sticker.svg";
  showMore: boolean = false;

  constructor() {
   
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if(this.selectedTip == 3){
      this.icon = "../../assets/food.svg"
      this.selectedTipText = "CabinShoe ile aldığınız her öneri sokak hayvanlarına yiyecek olarak dönüyor."
      this.showMore = true;
    }else{
      
    }
 
  }

  openUrl(){
    let url = "https://i.pinimg.com/originals/81/d7/aa/81d7aa53aa4254f0e8aba32e3847bcf3.jpg"
    window.open(url,'_blank');
  }
}
