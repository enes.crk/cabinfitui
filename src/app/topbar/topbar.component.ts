import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PrivacyPolicyComponent } from '../privacy-policy/privacy-policy.component';


@Component({
  selector: 'topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  @Input() logo;

  constructor(public dialog: MatDialog) { 

    // this.db.object('users').set({id:1, name: 'Enes Çırak'});
    // this.db.list('brands').valueChanges().subscribe(value => {
    //   console.log(value)
    // })
   

    
    
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PrivacyPolicyComponent, {
      width: "60%",
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
  }

  openOfficialWeb(){
    window.open('https://www.cabin.com.tr/')
  }
}
