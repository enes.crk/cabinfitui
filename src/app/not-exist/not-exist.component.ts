import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'not-exist',
  templateUrl: './not-exist.component.html',
  styleUrls: ['./not-exist.component.scss']
})
export class NotExistComponent implements OnInit {

  @Input() color;
  constructor() {
    if(!this.color){
      this.color = 'black';
    }
   }

  ngOnInit(): void {
  }

}
