
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { PageGuideComponent } from './pageGuide/pageGuide.component';
import { TopbarComponent } from './topbar/topbar.component';
import { TipComponent } from './tip/tip.component';
import { environment } from '../environments/environment';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { CdkTreeModule } from '@angular/cdk/tree';
import { PortalModule } from '@angular/cdk/portal';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule, MatSpinner } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTreeModule } from '@angular/material/tree';
import { MatBadgeModule } from '@angular/material/badge';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { InformationPageComponent } from './information-page/information-page.component';
import { ImageBoxComponent } from './image-box/image-box.component';
import { BraSizePageComponent } from './bra-size-page/bra-size-page.component';
import { BraDialogComponent } from './bra-dialog/bra-dialog.component';
import { BodyImageSelectionPageComponent } from './body-image-selection-page/body-image-selection-page.component';
import { RecommendationPageComponent } from './recommendation-page/recommendation-page.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { EnterMeasureModalComponent } from './enter-measure-modal/enter-measure-modal.component';
import { IntroPageComponent } from './intro-page/intro-page.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { NotExistComponent } from './not-exist/not-exist.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ManuelMeasureComponent } from './manuel-measure/manuel-measure.component'


const materialModules = [
  CdkTreeModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDividerModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatButtonToggleModule,
  MatTreeModule,
  OverlayModule,
  PortalModule,
  MatBadgeModule,
  MatGridListModule,
  MatRadioModule,
  MatDatepickerModule,
  MatTooltipModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule
];

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    PageGuideComponent,
    TipComponent,
    InformationPageComponent,
    AppComponent,
    TopbarComponent,
    TipComponent,
    ImageBoxComponent,
    BraSizePageComponent,
    BraDialogComponent,
    BodyImageSelectionPageComponent,
    RecommendationPageComponent,
    EnterMeasureModalComponent,
    IntroPageComponent,
    PrivacyPolicyComponent,
    NotExistComponent,
    ManuelMeasureComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule,
    ...materialModules


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
