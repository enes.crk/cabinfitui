import { HttpClient, HttpHeaders } from "@angular/common/http";
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import { ErrorStateMatcher } from "@angular/material/core";
import { BaseService } from "../services/base.service";
import { Gender } from "../data/constants/enums";
import { MatDialog } from "@angular/material/dialog";
import { ManuelMeasureComponent } from "../manuel-measure/manuel-measure.component";

@Component({
  selector: "information-page",
  templateUrl: "./information-page.component.html",
  styleUrls: ["./information-page.component.scss"],
})
export class InformationPageComponent implements OnInit {
  authorization: string = "1a0e12a3-147d-4f5a-bdae-dc588293b91e";

  customerId: number = null;
  customerAge: string = null; //* İsmi değiştirirsen mobile methodu değiştir.
  customerHeight: string = null;
  customerWeight: string = null;
  customerFt: string = null;
  customerIn: string = null;
  customerSt: string = null;
  customerLbs: string = null;
  genderId: number = null;

  minAge: number = 16;
  maxAge: number = 90;
  minHeight: number = 140;
  maxHeight: number = 200; //TODO: Karı 185
  minWeight: number = 40;
  maxWeight: number = 180; //TODO: Kadın 172

  hasAgeError: boolean;
  ageErrorMessage: string;
  hasHeightError: boolean;
  heightErrorMessage: string;
  hasWeightError: boolean;
  weightErrorMessage: string;
  hasFtError: boolean;
  ftErrorMessage: string;
  hasInError: boolean;
  inErrorMessage: string;
  hasStError: boolean;
  stErrorMessage: string;
  hasLbsError: boolean;
  lbsErrorMessage: string;
  requesCount: number = 0;

  isMetric: boolean = true;
  showButtonSpinner: boolean = false;

  imperialFt: number;
  imperialIn: number;
  imperialSt: number;
  imperialLbs: number;

  borderFemaleStyle: string = "none";
  borderMaleStyle: string = "none";
  femaleOpacity: string = "0.7";
  maleOpacity: string = "0.7";
  tooltip :string = "Ölçülerinizi girerek yüksek doğrulukta beden önerisi alabilirsin"
  ageOptions = [];
  heightOptions = [];
  weightOptions = [];
  defaultAge:number = null;
  defaultHeight:number = 170;
  defaultWeight: number = 70;
  manuelModel : any;
  showManuelMeasurePage: boolean = false;
  @Input() color;
  @Input() data;
  @Input() pageGuideTexts;
  @Input() brandId;
  @Input() cabinProductId;
  @Input() showGenderSelection;
  @Input() genderIdFromUrl;
  @Input() showMetric;
  @Input() isMobile;
  @Input() companyUserId;
  @Input() isDev;
  @Output() responseModel: EventEmitter<any> = new EventEmitter();
  @Output() userId: EventEmitter<any> = new EventEmitter();
  @Output() customerGender: EventEmitter<any> = new EventEmitter();
  componentHeight: string = null;
  hasManuel: boolean = false;

  constructor(
    private httpClient: HttpClient,
    private baseService: BaseService,
    private error: ErrorStateMatcher,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
   
    
    if (this.genderIdFromUrl) {
      this.genderId = this.genderIdFromUrl;
    }
    this.prepareOptions();
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.isGoToNextStageDisable();
  }

  // Gets Request Headers
  getRequestHeaders() {
    let requestHeaders = {
      Authorization: `Bearer ${this.authorization}`,
      "Brand-Id": `${this.brandId}`,
      "Product-Id": `${this.cabinProductId}`,
      "User-Id": `${this.customerId}`,
      "Brand-User-Id": `${this.companyUserId}`
    };

    return requestHeaders;
  }

  // Sets Gender Id
  setGender(genderId) {
    if (genderId == Gender.FEMALE) {
      this.genderId = Gender.FEMALE;
      this.customerGender.emit(this.genderId);

      this.borderFemaleStyle = `2px solid ${this.color}`;
      this.borderMaleStyle = `none`;
      this.femaleOpacity = "1";
      this.maleOpacity = "0.7";
    } else if (genderId == Gender.MALE) {
      this.genderId = Gender.MALE;
      this.customerGender.emit(this.genderId);

      this.borderMaleStyle = `2px solid ${this.color}`;
      this.borderFemaleStyle = `none`;
      this.femaleOpacity = "0.7";
      this.maleOpacity = "1";
    }
  }

  // Sets Customer Age
  setCustomerAge(event) {
    let age = event.target.value;
    setTimeout(() => {
      if (age >= this.minAge && age <= this.maxAge) {
        this.customerAge = event.target.value;
        this.hasAgeError = false;
      } else {
        this.customerAge = null;
        this.hasAgeError = true;
        this.ageErrorMessage = `Girilen yaş ${this.minAge}-${this.maxAge} aralığında olmalıdır`;
      }
    }, 400);
  }

  onCustomerSelectionChangeOnMobile(event, infoType: string) {
    switch (infoType) {
      case "age":
        this.customerAge = event.value;
        break;
      case "height":
        this.customerHeight = event.value;
        break;
      case "weight":
        this.customerWeight = event.value;
        break;
      default:
        break;
    }
  }
  // Sets Customer Height
  setCustomerHeight(event) {
    setTimeout(() => {
      let height = event.target.value;
      if (height >= this.minHeight && height <= this.maxHeight) {
        this.customerHeight = event.target.value;
        this.hasHeightError = false;
      } else {
        this.customerHeight = null;
        this.hasHeightError = true;
        this.heightErrorMessage = `Girilen boy ${this.minHeight}-${this.maxHeight} aralığında olmalıdır`;
      }
    }, 400);
  }

  // Sets Customer Weight
  setCustomerWeight(event) {
    setTimeout(() => {
      let weight = event.target.value;
      if (weight >= this.minWeight && weight <= this.maxWeight) {
        this.customerWeight = event.target.value;
        this.hasWeightError = false;
      } else {
        this.customerWeight = null;
        this.hasWeightError = true;
        this.weightErrorMessage = `Girilen kilo ${this.minWeight}-${this.maxWeight} aralığında olmalıdır`;
      }
    }, 400);
  }

  setImperial(event, type) {
    setTimeout(() => {
      if (type == "Ft") {
        if (event.target.value < 4 || event.target.value > 7) {
          this.hasFtError = true;
          this.ftErrorMessage =
            "Girilen değer 3 Ft 0 In ile 7 Ft 0 In arasında olmalıdır";
        } else {
          this.customerFt = event.target.value;
          this.hasFtError = false;
        }
      } else if (type == "In") {
        this.customerIn = event.target.value;
      } else if (type == "St") {
        if (event.target.value < 4 || event.target.value > 32) {
          this.hasStError = true;
          this.stErrorMessage =
            "Girilen değer 3 st 13 Lbs ile 31 St 7 Lbs arasında olmalıdır";
        } else {
          this.hasStError = false;
          this.customerSt = event.target.value;
        }
      } else if (type == "Lbs") {
        this.customerLbs = event.target.value;
      }
    }, 400);
  }

  // Sets User to Local Storage
  setUserId(data) {
    this.customerId = data.Id;
    let user = { Id: data.Id };

    window.localStorage.setItem("user", JSON.stringify(user));
  }

  // Gets Required Bodyparts
  getRequiredBodyPart() {
    let headers = this.getRequestHeaders();

    // Service Call
    this.httpClient
      .get(`${this.baseService.apiUrl}/Bodypart/Required`, { headers })
      .subscribe({
        next: (data) => {
          this.userId.emit(this.customerId);
          this.responseModel.emit(data[0]);
        },
        error: (error) => {},
      });
  }

  // Checks If "Go Next Stage" is Disable
  isGoToNextStageDisable() {
    if (this.isMetric) {
      if (
        !this.customerAge ||
        !this.customerHeight ||
        !this.customerWeight ||
        !this.genderId
      ) {
        return true;
      }
    } else if (!this.isMetric) {
      if (
        !this.customerAge ||
        !this.customerFt ||
        !this.customerIn ||
        !this.customerSt ||
        !this.customerLbs ||
        !this.genderId
      ) {
        return true;
      }
    } else {
      return false;
    }
  }

  // Goes to Next Stage
  goToNextStage() {
    this.requesCount++;
    // Prepare Data and Send Create User Request
    let headers = this.getRequestHeaders();
    let reqModel = {
      Gender: this.genderId,
      Age: parseInt(this.customerAge),
      Weight: this.isMetric
        ? parseInt(this.customerWeight)
        : Math.floor(
            parseInt(this.customerSt) * 6.35 +
              parseInt(this.customerLbs) * 0.453
          ),
      Height: this.isMetric
        ? parseInt(this.customerHeight)
        : Math.floor(
            parseInt(this.customerFt) * 30.48 + parseInt(this.customerIn) * 2.54
          ),
      IsActive: true,
    };

    if (this.requesCount == 1) {
      this.showButtonSpinner = true;
      // Service Call
      this.httpClient
        .post(`${this.baseService.apiUrl}/User`, reqModel, { headers })
        .subscribe({
          next: (data) => {
            this.showButtonSpinner = false;
            this.setUserId(data);
            if(this.hasManuel == true){
              this.postCustomerManuleMeasure()
            }else{
              this.getRequiredBodyPart();
            }
            
          },
          error: (error) => {},
        });
    }
  }
  prepareOptions() {
    for (let index = this.minAge; index <= this.maxAge; index++) {
      this.ageOptions.push(index);
    }
    for (let index = this.minHeight; index <= this.maxHeight; index++) {
      this.heightOptions.push(index);
    }
    for (let index = this.minWeight; index <= this.maxWeight; index++) {
      this.weightOptions.push(index);
    }
  }



  postCustomerManuleMeasure(){
    let headers = this.getRequestHeaders()
    this.httpClient
    .post(`${this.baseService.apiUrl}/Bodypart`, this.manuelModel, { headers })
    .subscribe({
      next: (data) => {
        this.getRequiredBodyPart();
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  openManuelPage(){
    this.showManuelMeasurePage = true;
  }

  setManuelInputa(event){
    this.manuelModel = event;
    this.hasManuel = true;
    this.goToNextStage()
  }

  backToInformationPage(event){
    this.showManuelMeasurePage = event;
  }
 
}
