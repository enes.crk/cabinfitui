import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'pageGuide',
  templateUrl: './pageGuide.component.html',
  styleUrls: ['./pageGuide.component.scss']
})
export class PageGuideComponent implements OnInit {

  @Input() pageGuideTexts;
  @Input() color;
  backgoundColor;

  constructor() 
  { 
    
  }

  ngOnInit(): void {
    this.backgoundColor = this.color
  }

}
