import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuelMeasureComponent } from './manuel-measure.component';

describe('ManuelMeasureComponent', () => {
  let component: ManuelMeasureComponent;
  let fixture: ComponentFixture<ManuelMeasureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManuelMeasureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuelMeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
