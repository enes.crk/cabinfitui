import { HttpClient } from "@angular/common/http";
import { Component, EventEmitter, Inject, Input, OnInit, Output } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DialogData } from "../bra-size-page/bra-size-page.component";
import { BaseService } from "../services/base.service";

@Component({
  selector: "manuel-measure",
  templateUrl: "./manuel-measure.component.html",
  styleUrls: ["./manuel-measure.component.scss"],
})

// export class measure {
// manChest:number;
// womanTopChest: number;
// womanBottomChest: number;
// waist: number;
// hip: number;
// }
export class ManuelMeasureComponent implements OnInit {
  waist: string;
  hip: string;
  isMetric: boolean = true;
  manChest: string;
  womanTopChest: string;
  womanBottomChest: string;
  hasChestError: boolean;
  hasBottomChestError: boolean;
  hasTopChestError: boolean;
  hasWaistError: boolean;
  hasHipError: boolean;
  chestErrorMessage: string;
  topChestErrorMessage: string;
  bottomChestErrorMessage: string;
  waistErrorMessage: string;
  hipErrorMessage: string;
  notEmpty: boolean = false;
  requestCount: number = 0;
  minChest: number = 50;
  maxChest: number = 160;
  minTopChest: number = 50;
  maxTopChest: number = 160;
  minBottomChest: number = 50;
  maxBottomChest: number = 160;
  minWaist: number = 50;
  maxWaist: number = 160;
  minHip: number = 50;
  maxHip: number = 160;

  @Input() gender;
  @Input() color;
  @Output() manuelModel: EventEmitter<any> = new EventEmitter();
  @Output() informationPage: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  setCustomerMeasure(event, type) {
    let value = parseInt(event.target.value)
    switch (type) {

      case "manChest":
        if(value > this.minChest && value < this.maxChest){
          this.manChest = event.target.value;
          this.notEmpty = true;
          this.hasChestError = false;
        }else{
          if(value == null){
            this.hasChestError = false;
          }else{
            this.hasChestError = true;
          }
         
          this.chestErrorMessage = `Değer ${this.minChest} ile ${this.maxChest} arasında olmalı.`
        }
   
        break;
      case "womanTopChest":
        if(value > this.minTopChest && value < this.maxTopChest){
          this.womanTopChest = event.target.value;
          this.notEmpty = true;
          this.hasTopChestError = false;
        }else{
          if(value == null){
            this.hasTopChestError = false;
          }else{
            this.hasTopChestError = true;
          }
          this.topChestErrorMessage = `Değer ${this.minTopChest} ile ${this.maxTopChest} arasında olmalı.`
        }
        break;
      case "womanBottomChest":
        if(value > this.minBottomChest && value < this.maxBottomChest){
          this.womanBottomChest = event.target.value;
          this.notEmpty = true;
          this.hasBottomChestError = false;
        }else{
          if(value == null){
            this.hasBottomChestError = false;
          }else{
            this.hasBottomChestError = true;
          }
          this.bottomChestErrorMessage = `Değer ${this.minBottomChest} ile ${this.maxBottomChest} arasında olmalı.`
        }
        break;
      case "waist":
       
        if(value > this.minWaist && value < this.maxWaist){
          this.waist = event.target.value;
          this.notEmpty = true;
          this.hasWaistError = false;
        }else{
          if(value == null){
            this.hasWaistError = false;
          }else{
            this.hasWaistError = true;
          }
          this.waistErrorMessage = `Değer ${this.minWaist} ile ${this.maxWaist} arasında olmalı.`
        }
        break;
      case "hip":
        if(value > this.minHip && value < this.maxHip){
          this.hip = event.target.value;
          this.notEmpty = true;
          this.hasHipError = false;
        }else{
          if(value == null){
            this.hasHipError = false;
          }else{
            this.hasHipError = true;
          }
          this.hipErrorMessage = `Değer ${this.minHip} ile ${this.maxHip} arasında olmalı.`
        }
        break;

      default:
        break;
    }
  }

 

  save() {
    this.requestCount++;

    let reqModel = [
    ];

    if(this.manChest){
      reqModel.push({"Id": 1, "Value": this.manChest})
    }
    if(this.womanTopChest){
      reqModel.push({"Id": 1, "Value":this.womanTopChest})
    }
    if(this.womanBottomChest){
      reqModel.push({"Id": 1, "Value": this.womanBottomChest})
    }
    if(this.waist){
      reqModel.push({"Id": 2, "Value": this.waist})
    }
    if(this.hip){
      reqModel.push({"Id": 3, "Value": this.hip})
    }
    if (this.requestCount == 1) {
      this.manuelModel.emit(reqModel);
       }
 
  }
  isDisabled(){
    if(this.gender == 1){
      if((!this.womanTopChest || !this.womanBottomChest) && !this.waist && !this.hip){
        return true
      }else{
        return false
      }
    }else{
      if(!this.manChest && !this.waist && !this.hip){
        return true
      }else{
        return false
      }
    }
  }

  goToInformationPage(){
    this.informationPage.emit(false);
  }
}
