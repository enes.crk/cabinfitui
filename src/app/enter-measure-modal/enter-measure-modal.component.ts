import { Component, Inject, Input, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DialogData } from "../bra-size-page/bra-size-page.component";

@Component({
  selector: "enter-measure-modal",
  templateUrl: "./enter-measure-modal.component.html",
  styleUrls: ["./enter-measure-modal.component.scss"],
})
export class EnterMeasureModalComponent implements OnInit {

  inputMeasure:number = null;

  constructor(public dialogRef: MatDialogRef<EnterMeasureModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

  ngOnInit(): void {
  }

  sendMeasure(): void {
    //TODO: Girilen ölçü istek olarak atılacak.
    this.dialogRef.close();
  }
}
