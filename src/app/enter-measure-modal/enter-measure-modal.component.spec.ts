import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterMeasureModalComponent } from './enter-measure-modal.component';

describe('EnterMeasureModalComponent', () => {
  let component: EnterMeasureModalComponent;
  let fixture: ComponentFixture<EnterMeasureModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnterMeasureModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterMeasureModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
