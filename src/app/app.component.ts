import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Component, SimpleChanges } from "@angular/core";
import { BaseService } from "./services/base.service";
import * as pageGuideData from "./data/json/pageGuideData.json";
import { Gender, Bodypart, UIPages } from "./data/constants/enums";
import { initializeApp } from 'firebase/app';
import { getDatabase, ref, child, get } from "firebase/database";
// import { BaseService } from "./services/base.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  firebaseConfig = {
    apiKey: "AIzaSyC9zcYdzopEb0-xoZCjFfpWbHoVJijzcSE",
    authDomain: "cabinfit-76461.firebaseapp.com",
    projectId: "cabinfit-76461",
    storageBucket: "cabinfit-76461.appspot.com",
    messagingSenderId: "526405516362",
    appId: "1:526405516362:web:429455acb9e62907d57ab3",
    measurementId: "G-ZJQXHHV6ZQ",
    databaseURL: "https://cabinfit-76461-default-rtdb.europe-west1.firebasedatabase.app/"
  };

  // Initialize Firebase
  app = initializeApp(this.firebaseConfig);
  dbRef = ref(getDatabase());

  authorization: string = "1a0e12a3-147d-4f5a-bdae-dc588293b91e";
  customColor = "#00C3B6";
  queryString = window.location.search;
  urlParams = new URLSearchParams(this.queryString);
  pageGuideTexts: any;

  showPageGuide: boolean = false;
  showInformationPage: boolean = false;
  showBraSizePage: boolean = false;
  showManChestPage: boolean = false;
  showWaistPage: boolean = false;
  showHipPage: boolean = false;
  showRecommendation: boolean = false;
  showNotExistPage: boolean = false;
  showGenderSelection: boolean = true;

  bodyPartId: number;
  brandId: number;
  cabinProductId: number = null;
  customerId: any;
  genderId: number = null;
  productId: string;
  productUrl: string = "";

  successAlert = false;
  selectedTip: any = "";
  tipIndex: number = 0;
  customerLogoUrl: string = "";
  turnBack: boolean = true;
  showSpinner: boolean = false;
  targetGender: string;
  resModel: any = [];
  lastPageIndex: any;
  calculatedResModel: any = [];
  showNotSuitable: boolean;
  companyUserId: string;
  customFont: string = "Quicksand";
  showMetric: boolean = false;
  isMobile: boolean;
  componentHeight: string;
  isDev: boolean;
  brandPreferences: object = {};

  // ?productId=shirtWebsite&customColor=rgb(242,%20132,%20130)&brand=1&poductUrl=https://st.mngbcn.com/rcs/pics/static/T8/fotos/S20/87017132_56.jpg?ts=1614092690530&imwidth=637&imdensity=1&gender=2

  constructor(
    private httpClient: HttpClient,
    private baseService: BaseService
  ) {
    if (window.screen.width < 600) {
      this.componentHeight = `100vh`
    } else {
      this.componentHeight = '600px'
    }
    this.getDeviceInformation();
    // URL Custom Color Controls
    if (this.urlParams.has("customColor")) {
      this.customColor = this.urlParams.get("customColor");
    }
    // Dev enviornment
    if (this.urlParams.has("isDev")) {
      this.isDev = true;
    }

    //* Url Company User ID
    if (this.urlParams.has("companyUserId")) {
      this.companyUserId = this.urlParams.get("companyUserId");
    }
    // Url .IsMeric
    if (this.urlParams.has("isMetric")) {
      if (this.urlParams.get("isMetric") == "true") {
        this.showMetric = true;
      }
    }
    // URL BrandId Controls
    if (this.urlParams.has("brand")) {
      // URL ProductId Controls
      if (this.urlParams.has("productId")) {
        this.brandId = parseInt(this.urlParams.get("brand"));
        //* BrandId setlendikten sonra marka tercihlerini firebase den çekmek için istek atar
        this.getBrandPreferenceFromFirebaseDb(this.brandId);
        this.productId = this.urlParams.get("productId");
        this.pageGuideTexts = pageGuideData.informationPage;
        // URL GenderId Controls
        if (this.urlParams.has("gender")) {
          let tempGenderId = parseInt(this.urlParams.get("gender"));
          if (tempGenderId == Gender.FEMALE || tempGenderId == Gender.MALE) {
            this.showGenderSelection = false;
            this.genderId = tempGenderId;
          }
        }
        // URL Product Url Controls
        if (this.urlParams.has("poductUrl")) {
          this.productUrl = this.urlParams.get("poductUrl");
        }
        // Check If Product Is Available
        this.getProductAvailability();
      } else {
        this.showNotExistPage = true;
      }
    } else {
      this.showNotExistPage = true;
    }
  }


  getBrandPreferenceFromFirebaseDb(brandId) {

    get(child(this.dbRef, `Brands/${brandId - 1}`)).then((snapshot) => {
      if (snapshot.exists()) {
        this.brandPreferences = snapshot.val();
        console.log(this.brandPreferences);
      }
    }).catch((error) => {
      console.error(error);
    });

  }
  // Gets Request Headers
  getRequestHeaders() {
    let requestHeaders = {
      Authorization: `Bearer ${this.authorization}`,
      "Brand-Id": `${this.brandId}`,
      "Product-Id": `${this.cabinProductId}`,
      "User-Id": `${this.customerId}`,
      "Brand-User-Id": `${this.companyUserId}`
    };

    return requestHeaders;
  }

  // Gets Product Availability Info
  getProductAvailability() {
    this.showSpinner = true;

    //* Bu istekte header ı dinamik almamamızın sebebi bu isteğin headerında brand-user-ıd gönderilmemei gerektiğidir.
    let headers = {
      Authorization: `Bearer ${this.authorization}`,
      "Brand-Id": `${this.brandId}`,
      "Product-Id": `${this.cabinProductId}`,
      "User-Id": `${this.customerId}`,
    };

    // Service Call
    this.httpClient
      .get(`${this.baseService.apiUrl}/Product/${this.productId}/IsAvailable`, {
        headers,
      })
      .subscribe({
        next: (data) => {
          this.showSpinner = false;
          // Check If Product Is Available
          this.isProductAvailable(data);
        },
        error: (error) => {
          this.handleOnResponseError();
        },
      });
  }

  // Check If Product Is Available
  isProductAvailable(data) {
    if (data.IsAvailable) {
      this.cabinProductId = data.Product.Id;

      if (data.Brand) this.customerLogoUrl = data.Brand.LogoUrl;

      // Check If User Exists in Local Storage
      if (this.isLocalUserExist()) {
        this.getRequiredBodyPart();
      } else {
        let res = {
          Id: 999,
          brandId: this.brandId,
          productId: this.cabinProductId,
        };
        this.getBodyPartOptions(res);
      }
    } else {
      this.handleOnResponseError();
    }
  }

  // Check If User Exists in Local Storage
  isLocalUserExist() {
    if (localStorage) {
      if (localStorage.getItem("user")) {
        this.customerId = JSON.parse(localStorage.getItem("user")).Id;
        console.log(this.customerId)
        return true;
      }
    } else {
      return false;
    }

  }

  // Gets Required Bodyparts
  getRequiredBodyPart() {
    this.showSpinner = true;
    let headers = this.getRequestHeaders();

    this.httpClient
      .get(`${this.baseService.apiUrl}/Bodypart/Required`, { headers })
      .subscribe({
        next: (data) => {
          this.showSpinner = false;
          this.setResponseModel(data[0]);
        },
        error: (error) => {
          this.resetAll();
        },
      });
  }

  // Gets Bodypart Options
  getBodyPartOptions(res) {
    if (res == undefined) {
      let visiblePage = { Id: 4 };
      this.setVisiblePage(visiblePage);
    } else {
      if (res.Id == 999) {
        this.setVisiblePage(res);
      } else {
        this.showSpinner = true;
        let headers = this.getRequestHeaders();

        this.httpClient
          .get(`${this.baseService.apiUrl}/Bodypart/${res.Id}/Options`, {
            headers,
          })
          .subscribe({
            next: (data) => {
              this.showSpinner = false;
              this.resModel = data;
              this.setVisiblePage(res);
            },
            error: (error) => {
              this.showSpinner = false;
              this.resetAll();
            },
          });
      }
    }
  }

  // Sets All Page Visibilities to False
  setAllPageVisibilitiesToFalse() {
    this.showInformationPage = false;
    this.showBraSizePage = false;
    this.showManChestPage = false;
    this.showWaistPage = false;
    this.showHipPage = false;
    this.showRecommendation = false;
  }

  // Sets Relevant Pages Visibility to True
  setVisiblePage(res?) {
    switch (res.Id) {
      case UIPages.INFORMATION_PAGE:
        this.setAllPageVisibilitiesToFalse();
        this.turnBack = false;
        this.showInformationPage = true;
        this.pageGuideTexts = pageGuideData.informationPage;
        this.showPageGuide = true;
        break;
      case UIPages.CHEST_PAGE:
        this.setAllPageVisibilitiesToFalse();
        this.turnBack = true;
        if (this.genderId == Gender.FEMALE) {
          this.showBraSizePage = true;
          this.pageGuideTexts = pageGuideData.braSizePage;
        } else if (this.genderId == Gender.MALE) {
          this.bodyPartId = Bodypart.CHEST;
          this.showManChestPage = true;
          this.pageGuideTexts = pageGuideData.manChestPage;
        }
        this.showPageGuide = true;
        break;
      case UIPages.WAIST_PAGE:
        this.setAllPageVisibilitiesToFalse();
        this.bodyPartId = Bodypart.WAIST;
        this.showWaistPage = true;
        this.pageGuideTexts = pageGuideData.waistPage;
        this.showPageGuide = true;
        break;
      case UIPages.HIP_PAGE:
        this.setAllPageVisibilitiesToFalse();
        this.bodyPartId = Bodypart.HIP;
        this.showHipPage = true;
        this.pageGuideTexts = pageGuideData.hipPage;
        this.showPageGuide = true;
        break;
      case UIPages.RECOMMENDATION_PAGE:
        this.setAllPageVisibilitiesToFalse();
        this.turnBack = true;
        this.showRecommendation = true;
        this.pageGuideTexts = pageGuideData.recommendationPage;
        this.showPageGuide = true;
        break;
      default:
        this.setAllPageVisibilitiesToFalse();
        this.turnBack = false;
        this.showInformationPage = true;
        this.pageGuideTexts = pageGuideData.informationPage;
        this.showPageGuide = true;
        break;
    }
  }

  // Calls Required Bodyparts or Calculate Fitness Service
  setResponseModel(res) {
    if (res == undefined) {
      this.getCalculateFitness();
    } else {
      this.getBodyPartOptions(res);
    }
  }

  // Gets Calculated Fitness
  getCalculateFitness() {
    let headers = this.getRequestHeaders();

    this.httpClient
      .get(`${this.baseService.apiUrl}/Fitness`, { headers })
      .subscribe({
        next: (data) => {
          this.calculatedResModel = data;
          let visiblePage = { Id: 4 };
          this.setVisiblePage(visiblePage);
        },
        error: (error) => {
          this.resetAll();
        },
      });
  }

  // set user isActive false method
  setUserIsActiveFalse() {
    let headers = this.getRequestHeaders();
    let reqModel = {
      Id: this.customerId,
      Gender:
        this.calculatedResModel != []
          ? this.calculatedResModel.User
            ? this.calculatedResModel.User.Gender
            : null
          : this.genderId
            ? this.genderId
            : null,
      Age:
        this.calculatedResModel != []
          ? this.calculatedResModel.User
            ? this.calculatedResModel.User.Age
            : null
          : null,
      Weight:
        this.calculatedResModel != []
          ? this.calculatedResModel.User
            ? this.calculatedResModel.User.Weight
            : null
          : null,
      Height:
        this.calculatedResModel != []
          ? this.calculatedResModel.User
            ? this.calculatedResModel.User.Height
            : null
          : null,
      IsActive: false,
    };

    // Service Call
    this.httpClient
      .patch(`${this.baseService.apiUrl}/User`, reqModel, { headers })
      .subscribe({
        next: (data) => {
          window.localStorage.removeItem("user");
          this.showSpinner = false;
        },
        error: (error) => { },
      });
  }
  // Handles On Response Throws Error
  handleOnResponseError() {
    this.showNotExistPage = true;
    this.showSpinner = false;
    this.turnBack = false;
    this.showPageGuide = false;
  }

  //#region CALLS FROM HTML
  // Sets Customer Gender
  setCustomerGender(event) {
    this.genderId = event;
  }

  // Sets User Id
  setUserId(event) {
    this.customerId = event;
  }

  // Directs to Previous Page
  goToPreviousPage(event) {
    if (event == true) {
      this.lastPageIndex = this.lastPageIndex - 1;
      this.setVisiblePage(this.lastPageIndex);
    }
  }

  // Clears User from Cache and Resets Processes
  resetAll() {
    this.setUserIsActiveFalse();
    this.setAllPageVisibilitiesToFalse();
    let visiblePage = { visiblePageId: 1 };
    this.setVisiblePage(visiblePage);
    this.showNotSuitable = false;
  }
  //#endregion CALLS FROM HTML

  setSuitable(event) {
    if (event == true) {
      this.showNotSuitable = true;
    }
  }
  // Closes Iframe
  closeIframe() {
    console.log("Close");
    // Ana siteye iframe i kapatmasını söyler.
    parent.postMessage("closeCabinFitIframe", "*");
  }
  getDeviceInformation() {
    this.isMobile = window.outerWidth <= 600 ? true : false;
  }
}
