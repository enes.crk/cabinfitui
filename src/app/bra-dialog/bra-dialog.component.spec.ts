import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BraDialogComponent } from './bra-dialog.component';

describe('BraDialogComponent', () => {
  let component: BraDialogComponent;
  let fixture: ComponentFixture<BraDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BraDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BraDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
