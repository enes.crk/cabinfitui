import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../bra-size-page/bra-size-page.component';

@Component({
  selector: 'app-bra-dialog',
  templateUrl: './bra-dialog.component.html',
  styleUrls: ['./bra-dialog.component.scss']
})
export class BraDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<BraDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
