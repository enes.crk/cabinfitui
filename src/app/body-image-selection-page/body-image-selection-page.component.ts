import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseService } from '../services/base.service';

@Component({
  selector: 'body-image-selection-page',
  templateUrl: './body-image-selection-page.component.html',
  styleUrls: ['./body-image-selection-page.component.scss']
})
export class BodyImageSelectionPageComponent implements OnInit {

  @Input() color;
  @Input() pageGuideTexts;
  @Input() data;
  @Input() brandId;
  @Input() productId;
  @Input() customerId;
  @Input() bodyPartId;
  @Input() companyUserId;
  @Output() responseModel: EventEmitter<any> = new EventEmitter();

  res = {};
  imgIndex: number = 1;
  authorization: string = "1a0e12a3-147d-4f5a-bdae-dc588293b91e";
  requestCount: number = 0;

  constructor(private httpClient: HttpClient,  private baseService: BaseService) { }

  ngOnInit(): void { 
  }
  getRequestHeaders() {
    let requestHeaders = {
      Authorization: `Bearer ${this.authorization}`,
      "Brand-Id": `${this.brandId}`,
      "Product-Id": `${this.productId}`,
      "User-Id": `${this.customerId}`,
      "Brand-User-Id": `${this.companyUserId}`
    };

    return requestHeaders;
  }
  setSelectedImage(){

  }


  goToNextStage(value){
    this.requestCount++;

    let reqModel = [{
      "Id": this.bodyPartId,
      "Value": value
  }]

    var headers = this.getRequestHeaders();

    if(this.requestCount == 1){
      this.httpClient
      .post(
        `${this.baseService.apiUrl}/Bodypart`, reqModel,
        { headers }
      )
      .subscribe({
        next: (data) => {
          this.getRequiredBodyPart();
        },
        error: (error) => {
          console.log(error);
        },
      });
    }


    

  }
  getRequiredBodyPart(){
    var headers = this.getRequestHeaders();
    let res
    this.httpClient
      .get(
        `${this.baseService.apiUrl}/Bodypart/Required`,
        { headers }
      )
      .subscribe({
        next: (data) => {
          if(data[0] == undefined){
            let res = {Id:4, brandId: this.brandId, productId: this.productId};
            this.responseModel.emit(undefined);
          }else{
            res = data[0]
            this.responseModel.emit(res);
          }
         
         
        },
        error: (error) => {
          console.log(error);
        },
      });
      
     
  }
  pickImage(){
    for (let index = 0; index < this.data.Options.length; index++) {
      if(index == this.imgIndex){
       let value = this.data.Options[index].Value
       this.goToNextStage(value)
      }
    }
  }
}
