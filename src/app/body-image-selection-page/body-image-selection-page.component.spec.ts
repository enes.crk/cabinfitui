import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyImageSelectionPageComponent } from './body-image-selection-page.component';

describe('BodyImageSelectionPageComponent', () => {
  let component: BodyImageSelectionPageComponent;
  let fixture: ComponentFixture<BodyImageSelectionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BodyImageSelectionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyImageSelectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
