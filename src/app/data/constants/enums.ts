export enum Gender {
    FEMALE = 1,
    MALE = 2
}

export enum Bodypart {
    CHEST = 1,
    WAIST = 2,
    HIP = 3
}

export enum UIPages {
    INFORMATION_PAGE = 0,
    CHEST_PAGE = 1,
    WAIST_PAGE = 2,
    HIP_PAGE = 3,
    RECOMMENDATION_PAGE = 4
}